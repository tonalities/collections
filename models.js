import { walk } from 'https://deno.land/std/fs/mod.ts'

const getModels = async () => {
  const models = []
  for await (const file of walk(`music-analysis-ontology/annotationModels/JSON`)) {
    if (file.isFile)
      try {
        const model = JSON.parse(await Deno.readTextFile(file.path))
        const ontology = model.filter(c => c['@type'].includes('http://www.w3.org/2002/07/owl#Ontology'))[0]
        const description = ontology['http://www.w3.org/2000/01/rdf-schema#comment']?.find(e => e['@value'])['@value']
        const label = ontology['@id'].split('/').pop() || ontology['@id'].split('/').at(-2)
        const name = label.split('_').join(' ').split('-').join(' ')

        const classes = model.filter(c => c['@type'].includes('http://www.w3.org/2002/07/owl#Class'))
        const classesIri = classes.map(c => c['@id'])
        const classesWithParent = classes.map(c => {
          const isDisabled = !!c['http://data-iremus.huma-num.fr/ns/sherlock#musicAnnotationConcept']
          const parent = c['http://www.w3.org/2000/01/rdf-schema#subClassOf']?.find(p =>
            classesIri.includes(p['@id'])
          )?.['@id']

          return {
            iri: c['@id'],
            ...(parent && { parent }),
            ...(isDisabled && { isDisabled }),
          }
        })
        const rootClasses = classesWithParent.filter(c => !c.parent)

        const getSubClasses = iri => classesWithParent.filter(c => c.parent === iri).map(c => createNode(c))

        const createNode = ({ iri, isDisabled }) => {
          const subClasses = getSubClasses(iri)
          return {
            iri,
            ...(subClasses.length && { subClasses }),
            ...(isDisabled && { isDisabled }),
          }
        }
        
        const data = rootClasses.map(c => createNode(c))

        models.push({
          baseIri: ontology['@id'],
          name: name.charAt(0).toUpperCase() + name.slice(1),
          filename: file.path.split('/').pop(),
          ...(description && { description }),
          data,
        })
      } catch (error) {
        console.error(`Erreur lors de la lecture du fichier ${file.path}: ${error}`)
      }
  }
  return models
}

const writeModels = async () => {
  try {
    const models = JSON.stringify(await getModels())
    await Deno.writeTextFile('./public/models.json', models)
    console.log('File created successfully')
  } catch (error) {
    console.error('Error writing file:', error)
  }
}

writeModels()
