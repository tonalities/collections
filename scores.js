import { walk } from 'https://deno.land/std/fs/mod.ts'
import { xml2js } from 'https://deno.land/x/xml2js@1.0.0/mod.ts'

const composers = {
  Anonyme: 'Anonyme',
  Dufay: 'Guillaume Dufay',
  'Bach/Bach_Chorals': 'Johann Sebastian Bach',
  'Bach/Bach_Fugues': 'Johann Sebastian Bach',
  De_Mantua: 'Jacquet de Mantua',
  De_Rore: 'Cyprien de Rore',
  DeLaRue: 'Pierre de La Rue',
  Dufay: 'Guillaume Dufay',
  Fontanelli: 'Alfonso Fontanelli',
  Gombert: 'Nicolas Gombert',
  Hellinck: 'Lupus Hellinck',
  Isaac: 'Heinrich Isaac',
  'Josquin/Josquin_Chansons': 'Josquin des Prez',
  'Josquin/Josquin_Sacred_Music': 'Josquin des Prez',
  Lechner: 'Leonhard Lechner',
  Morales: 'Cristobal de Morales',
  'Praetorius/Terpsichore': 'Michael Prætorius',
  Verdelot: 'Philippe Verdelot',
  Willaert: 'Adrian Willaert',
  Zarlino: 'Gioseffo Zarlino',
}

const getScores = async composer => {
  const files = []
  for await (const file of walk(`tonalities_pilot/scores/${composer}`)) {
    if (file.isFile && file.name.endsWith('.mei'))
      try {
        const xml = await Deno.readTextFileSync(file.path)
        const obj = xml2js(xml, { compact: true })
        files.push({
          meiUrl: `https://raw.githubusercontent.com/polifonia-project/tonalities_pilot/main/scores/${composer}/${file.name}`,
          scoreTitle:
            obj.mei.meiHead.fileDesc?.titleStmt?.title?._text ||
            obj.mei.meiHead.fileDesc?.titleStmt?.title[0]?._text ||
            obj.mei.meiHead.workList?.work?.title?._text ||
            obj.mei.meiHead.workList?.work?.title[0]?._text ||
            file.name.split('_').join(' ').slice(0, -4),
          scoreComposer: composers[composer],
        })
      } catch (error) {
        console.error(`Erreur lors de la lecture du fichier ${file.path}: ${error}`)
      }
  }
  return files
}

const getAllScores = async composers => {
  try {
    const scores = []
    for await (const [key] of Object.entries(composers)) scores.push(...(await getScores(key)))
    await Deno.writeTextFile('./public/scores.json', JSON.stringify(scores))
    console.log('File created successfully')
  } catch (error) {
    console.error('Error writing file:', error)
  }
}

getAllScores(composers)
